#include <iostream>
#include "list.hpp"

int main() {

  List<int> moja_lista;

  moja_lista.push_back(3);
  moja_lista.push_back(4);
  moja_lista.push_back(5);
  moja_lista.push_front(2);
  moja_lista.push_front(1);

  std::cout << moja_lista.size() << " == 5" << std::endl;

  std::cout << moja_lista.at(0) << ", "
            << moja_lista.at(1) << ", "
            << moja_lista.at(2) << ", " 
            << moja_lista.at(3) << ", "
            << moja_lista.at(4) << std::endl;

  auto pprint = [](auto& lst) {
    std::cout << lst.at(0) << ", "
              << lst.at(1) << ", "
              << lst.at(2) << ", " 
              << lst.at(3) << ", "
              << lst.at(4) << std::endl;
  };
  // Pravilo petorke
  auto moja_lista2 = moja_lista;
  pprint(moja_lista2);
  auto moja_lista3 = std::move(moja_lista2);
  pprint(moja_lista3);
  moja_lista2 = std::move(moja_lista3);
  pprint(moja_lista2);
  moja_lista3 = moja_lista;
  pprint(moja_lista3);

  moja_lista2.pop_front();
  moja_lista2.pop_back();
  moja_lista2.pop_back();
  moja_lista2.pop_front();
  moja_lista2.pop_front();
  // Prazna lista
  for (size_t i = 0; i < moja_lista2.size(); i++) {
    std::cout << moja_lista2.at(i) << std::endl;
  }

  moja_lista.clear();
  for (size_t i = 0; i < moja_lista.size(); i++) {
    std::cout << moja_lista.at(i) << std::endl;
  }

  std::cout << "Iteratori" << std::endl;
  auto begin = moja_lista3.begin();
  std::cout << moja_lista3.at(0) << std::endl;
  std::cout << *begin << std::endl;
  std::cout << *++begin << std::endl;

  std::cout << "Iteratori 2" << std::endl;
  for (auto iter = moja_lista3.begin(); iter != moja_lista3.end(); ++iter) {
    std::cout << *iter << std::endl;
  }

}
