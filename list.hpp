#pragma once

#include <iostream>
#include <iterator>

template <typename T>
class List {
  public:
  List() = default;
  List(const List& other);
  List(List&& other);
  List& operator=(const List& other);
  List& operator=(List&& other);
  ~List();

  void clear();
  void push_back(const T& element);
  void push_front(const T& element);

  // void push_back(T&& element);
  // void push_front(T&& element);

  // T& front();
  // T& back();
  // T& front() const;
  // T& back() const;
  size_t size() const;
  bool empty() const;
  void pop_front();
  void pop_back();

  // slozenost??
  const T& at(size_t index) const;

  class iterator;
  iterator begin() {}
  iterator end() {}

  void insert(iterator position, const T& element);
  // void insert(iterator position, T&& element);

  void erase(iterator position);

  private:

  struct Node {
    T value;
    Node* next;
  };

  Node* head_ = nullptr;
  Node* tail_ = nullptr;
  size_t size_ = 0;

};


template <typename T>
class List<T>::iterator : public std::iterator<std::forward_iterator_tag, T> {
  public:
  friend class List<T>;

  // iterator(Node* p) : p_{p} {}

  iterator& operator++() {
  }

  iterator operator++(int) {
  }

  bool operator==(const iterator& other) {
  }

  bool operator!=(const iterator& other) {
  }

  T& operator*() {
  }

  // T* operator->() {
  // }

  private:
  // Node* p_;
};


template <typename T>
void List<T>::push_back(const T& element) {
  std::cout << "push_back const&" << std::endl;
  auto temp = new Node();
  temp->value = element;
  temp->next = nullptr;
  if (size_ == 0) {
    tail_ = head_ = temp;
  } else {
    tail_->next = temp;
    tail_ = temp;
  }
  size_++;
}

template <typename T>
void List<T>::push_front(const T& element) {
  std::cout << "push_front const&" << std::endl;
  auto temp = new Node{element, nullptr};
  if (size_) {
    temp->next = head_;
    head_ = temp;
  } else {
    tail_ = head_ = temp;
  }
  size_++;
}

// template <typename T>
// void List<T>::push_back(T&& element) {
//   std::cout << "push_back T&&" << std::endl;
// }

// template <typename T>
// void List<T>::push_front(T&& element) {
//   std::cout << "push_front T&&" << std::endl;
// }

template <typename T>
size_t List<T>::size() const {
  return size_;
}

template <typename T>
bool List<T>::empty() const {
  return size_ == 0;
}

// slozenost O(n)
template <typename T>
const T& List<T>::at(size_t index) const {
  if (index >= size_) throw std::out_of_range("Index error!");
  auto temp = head_;
  while (index--) {
    temp = temp->next;
  }
  return temp->value;
}

template <typename T>
void List<T>::clear() {
  while (head_ != nullptr) {
    auto temp = head_->next;
    delete head_;
    head_ = temp;
  }
  tail_ = nullptr;
  size_ = 0;
}


template <typename T>
List<T>::List(const List& other) {
  auto temp = other.head_;
  while (temp != nullptr) {
    push_back(temp->value);
    temp = temp->next;
  }
}
template <typename T>
List<T>::List(List&& other) 
: head_{other.head_}, tail_{other.tail_}, size_{other.size_}
{
  other.tail_ = other.head_ = 0;
  other.size_ = 0;
}

template <typename T>
List<T>& List<T>::operator=(const List& other) {
  if (this != &other) {
    clear();
    auto temp = other.head_;
    while (temp != nullptr) {
      push_back(temp->value);
      temp = temp->next;
    }
  }
  return *this;
}
template <typename T>
List<T>& List<T>::operator=(List&& other) {
  if (this != &other) {
    clear();
    head_ = other.head_;
    tail_ = other.tail_;
    size_ = other.size_;
    other.head_ = other.tail_ = nullptr;
    other.size_ = 0;
  }
  return *this;
}
template <typename T>
List<T>::~List() {
  clear();
}

template <typename T>
void List<T>::pop_front() {
  if (size_ == 0) return;

  auto temp = head_;
  head_ = head_->next;
  delete temp;
  size_--;

  if (size_ == 0) tail_ = nullptr;
}

template <typename T>
void List<T>::pop_back() {
  if (size_ == 0) return;
  if (size_ == 1) {
    pop_front();
  } else {
    auto temp = head_;
    while (temp->next->next != nullptr) {
      temp = temp->next;
    }
    delete tail_;
    tail_ = temp;
    temp->next = nullptr;
    size_--;
  }
}

// O(n)
template <typename T>
void List<T>::insert(iterator position, const T& element) {
}
// O(1)

// O(n)
template <typename T>
void List<T>::erase(iterator position) {
}
// O(1)??
