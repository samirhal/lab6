#include "list.hpp"

struct Complex {
  double re;
  double im;
};

int main() {

  List<Complex> brojevi; 
  brojevi.push_back(Complex{10.0, 20.0});
  brojevi.push_back(Complex{5.5, 0.2});
  brojevi.push_back(Complex{-8.3, 3.2});

  for (auto it = brojevi.begin(); it != brojevi.end(); it++) {
    it->im *= 2;
    it->re *= 2;
  }


  List<std::string> lista;
  lista.push_back("Strukture");
  lista.push_back("podataka");
  lista.push_back("2021");

  std::string unos;
  while (std::cin >> unos) {
    lista.push_back(std::move(unos));
  }
  lista.push_front("Unos korisnika: ");
  for (auto iter = lista.begin(); iter != lista.end(); ++iter) {
    std::cout << *iter << std::endl;
  }
}
