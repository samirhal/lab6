#pragma once

#include <iostream>
#include <iterator>

template <typename T>
class List {
  public:
  List() = default;

  void push_back(const T& element);
  void push_front(const T& element);
  void pop_front();
  void pop_back();

  size_t size() const;
  bool empty() const;
  const T& at(size_t index) const;

  private:

  struct Node {
    T value;
    Node* next;
  };

  Node* head_ = nullptr;
  Node* tail_ = nullptr;
  size_t size_ = 0;
};

template <typename T>
void List<T>::push_back(const T& element) {
  auto temp = new Node{element, nullptr};
  if (size_ == 0) {
    tail_ = head_ = temp;
  } else {
    tail_ = tail_->next = temp;
  }
  size_++;
}

template <typename T>
void List<T>::push_front(const T& element) {
  auto temp = new Node{element, nullptr};
  if (size_) {
    temp->next = head_;
    head_ = temp;
  } else {
    tail_ = head_ = temp;
  }
  size_++;
}

template <typename T>
size_t List<T>::size() const {
  return size_;
}

template <typename T>
bool List<T>::empty() const {
  return size_ == 0;
}

// slozenost O(n)
template <typename T>
const T& List<T>::at(size_t index) const {
  if (index >= size_) throw std::out_of_range("Index error!");
  auto temp = head_;
  while (index--) {
    temp = temp->next;
  }
  return temp->value;
}

template <typename T>
void List<T>::pop_front() {
  if (size_ == 0) return;

  auto temp = head_;
  head_ = head_->next;
  delete temp;
  size_--;

  if (size_ == 0) tail_ = nullptr;
}

template <typename T>
void List<T>::pop_back() {
  if (size_ == 0) return;
  if (size_ == 1) {
    pop_front();
  } else {
    auto temp = head_;
    while (temp->next->next != nullptr) {
      temp = temp->next;
    }
    delete tail_;
    tail_ = temp;
    temp->next = nullptr;
    size_--;
  }
}
