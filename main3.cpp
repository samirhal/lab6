#include "list.hpp"

int main() {
  List<std::string> lista;
  lista.push_back("Strukture");
  lista.push_back("podataka");
  lista.push_back("2023");

  auto pos = lista.begin();
  pos++;
  pos++;
  lista.insert(pos, "Zadaca");
  lista.insert(lista.begin(), "Poruka: ");
  lista.erase(++lista.begin());
  lista.erase(lista.begin());
  for (auto iter = lista.begin(); iter != lista.end(); ++iter) {
    std::cout << *iter << std::endl;
  }
}
